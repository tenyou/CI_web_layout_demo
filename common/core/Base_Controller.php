<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Public controller
 *
 * @see CI_Controller
 */
class Base_Controller extends CI_Controller
{
	/**
	 * construct
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		/*
		 * Whether the number of parameters is legitimate by reflection,
		 */
		$reflection = new ReflectionClass($this);
		$method_reflection = $reflection->getMethod($this->router->method);
		$argnum = count(array_slice($this->uri->rsegments, 2));
		
		if ($method_reflection->getNumberOfRequiredParameters() > $argnum
			|| $method_reflection->getNumberOfParameters() < $argnum)
		{
			show_404(get_class($this)."/{$this->router->method}");
		}
		
		// Load public class library or helper
		//$this->load->library('aaaa');
		$this->load->helper('core');
		$this->load->helper('autoload');
		$this->load->library('Template');
	}
}
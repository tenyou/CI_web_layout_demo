<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Exceptions Class
 *
 * 将错误显示模板文件放到/common/views/errors中,
 * 同时通过不同的环境(ENVIRONMENT)调用相应的错误提示页
 *
 * 注意：
 * 1、系统将默认首先调用应用文件夹(application/views/errors)下相应的错误提示页,
 * 如果不存在才会调用公用的提示页.
 * 2、应用文件夹(application/views/errors)下的提示页不区分环境(ENVIRONMENT)
 */
class Base_Exceptions extends CI_Exceptions
{
	/**
	 * General Error Page
	 *
	 * This function takes an error message as input
	 * (either as a string or an array) and displays
	 * it using the specified template.
	 *
	 * @access private
	 * @param string the heading
	 * @param string the message
	 * @param string the template name
	 * @param int the status code
	 * @return string
	 */
	function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		set_status_header($status_code);
		
		$message = '<p>' . implode('</p><p>', (!is_array($message)) ? array (
			$message 
		) : $message) . '</p>';
		
		if (ob_get_level() > $this->ob_level + 1) {
			ob_end_flush();
		}
		
		$file = APPPATH.'views/errors/'.$template.'.php';
		if ( ! file_exists($file))
		{
			$folder = ENVIRONMENT == 'development' ? 'development' : 'production';
			$file = COMPATH . 'views/errors/' . $folder . '/html/' . $template . '.php';
		}
		
		ob_start();
		include ($file);
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Native PHP error handler
	 *
	 * @access private
	 * @param string the error severity
	 * @param string the error string
	 * @param string the error filepath
	 * @param string the error line number
	 * @return string
	 */
	function show_php_error($severity, $message, $filepath, $line)
	{
		$severity = (!isset($this->levels[$severity])) ? $severity : $this->levels[$severity];
		
		$filepath = str_replace("\\", "/", $filepath);
		
		// For safety reasons we do not show the full file path
		if (FALSE !== strpos($filepath, '/')) {
			$x = explode('/', $filepath);
			$filepath = $x[count($x) - 2] . '/' . end($x);
		}
		
		if (ob_get_level() > $this->ob_level + 1) {
			ob_end_flush();
		}
		
		$file = APPPATH.'views/errors/error_php.php';
		if ( ! file_exists($file))
		{
			$folder = ENVIRONMENT == 'development' ? 'development' : 'production';
			$file = COMPATH . 'views/errors/' . $folder . '/html/error_php.php';
		}
		ob_start();
		include ($file);
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
	}
	
	/**
	 * CI新版新增的输出异常样式文件
	 *
	 * {@inheritdoc}
	 *
	 * @see CI_Exceptions::show_exception()
	 */
	public function show_exception($exception)
	{
		$templates_path = COMPATH . 'views/errors/';
		
		$message = $exception->getMessage();
		if (empty($message)) {
			$message = '(null)';
		}
		
		$templates_path .= ENVIRONMENT == 'development' ? 'development' . DIRECTORY_SEPARATOR : 'production' . DIRECTORY_SEPARATOR;
		if (is_cli()) {
			$templates_path .= 'cli' . DIRECTORY_SEPARATOR;
		} else {
			$templates_path .= 'html' . DIRECTORY_SEPARATOR;
		}
		
		if (ob_get_level() > $this->ob_level + 1) {
			ob_end_flush();
		}
		
		ob_start();
		include ($templates_path . 'error_exception.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
	}
}
// END Exceptions Class

/* End of file Exceptions.php */
/* Location: ./system/core/Exceptions.php */
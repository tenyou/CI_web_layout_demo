<?php
/**
 * 公共model，以表名命名的model
 * 表名：xxx_user
 */

class Com_user_model extends MY_Model
{
	/**
	 * Table name
	 *
	 * @var string
	 * @access protected
	 */
	public static $table_name = 'user';

	/**
	 *Table primary key, if no default is used "Id".
	 *
	 * @var string
	 * @access protected
	 */
	protected $key = 'uid';
	
	function com_get_user($uid)
	{
		return $this->find($uid);
	}
}


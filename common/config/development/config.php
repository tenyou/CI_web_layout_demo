<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// config domian,cookie_name and so on

$config['domian_www']    = 'http://www.xxx.com/';  // 首页域名
$config['cookie_name']    = 'ggg'; // cookie名
$config['cookie_domain'] = '.xxx.com'; // cookie域

// ……

$config['domain_static'] = 'http://static.xxx.com/'; // 静态站点域名
$config['domain_min_uri'] = 'http://static.xxx.com/min/?f='; // js/css压缩地址

$config['common_config'] = 'I\' m a common config value'; // cookie域
// require or include other config files, if need
//require dirname(__FILE__).DIRECTORY_SEPARATOR.'xxx_config.php';

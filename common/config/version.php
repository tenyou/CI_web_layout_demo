<?php

// web system version
define('WEB_VERSION', '1.0');
// web system update time
define('WEB_BUILD', '201805071552');

// 用于js、css文件请求参数，防止js、css有更新时，浏览器还是缓存之前的js、css内容导致网站有问题
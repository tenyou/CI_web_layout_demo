# CI_web_layout_demo

#### 项目介绍
CI框架web布局demo
1. 多站点布局
2. 简化数据库操作
3. 视图渲染layout

[查看站点nginx配置
](https://gitee.com/tenyou/CI_web_layout_demo/blob/master/docs/nginx/xxx.com.conf)

#### 使用要求

1. CI版本3.1.8(其他版本不保证能正常使用)
2. PHP版本5.5+

#### 数据库操作示例
以表名命名的model，继承公共model  Com_user_model，若没有公共model ，可直接继承 MY_Model
```
<?php
/**
 * 公共model，以表名命名的model
 * 表名：xxx_user
 */

class Com_user_model extends MY_Model
{
	/**
	 * Table name
	 *
	 * @var string
	 * @access protected
	 */
	public static $table_name = 'user';

	/**
	 *Table primary key, if no default is used "Id".
	 *
	 * @var string
	 * @access protected
	 */
	protected $key = 'uid';
	
	function com_get_user($uid)
	{
		return $this->find($uid);
	}
}

class User_model extends Com_user_model
{
	/**
	 * Database connection configuration
	 * default 可设置为主库，slave为从库
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $db_group = 'slave';

	/**
	 * 是否返回最后新增的ID，即insert后返回insert_id
	 *
	 * @var bool
	 */
	protected $return_insert_id = FALSE;
	
	function get_user($uid)
	{
		return $this->find($uid);
	}
}
```

 **假设需要查询uid为1的用户信息** 
```
// 使用原生$this->db操作
// 加载公共数据库配置，使用从库
$this->load->database('slave');
$userinfo = $this->db->select('*')->from('user')->where('uid', 1)->get()->row_array();

// 简化后如下，在controller中加载User_model，model自会加载公共数据库配置
$this->load->model('user_model', 'user');
// 等价于使用原生$this->db操作
$userinfo = $this->user->find(1);
```

 **也可以这样** 
```
$userinfo = $this->user->where('uid', 1)->find();
// 或者 指定字段,不指定默认select(*)
$userinfo = $this->user->select('uid,username')->where('uid', 1)->find();

// 使用model自身封装的方法
$userinfo = $this->user->get_user(1);
// 使用model继承的公共model中封装的方法
$userinfo = $this->user->com_get_user(1);
```

 **关于连表操作** 
```
// 原生$this->db连表查询
$this->db->select('user.uid as uid1, user2.uid as uid2');
$userinfo = $this->db->join('user2', 'user.uid=user2.uid','left')->where('user2.uid', 1)->get()->row_array();

// 使用set_table_alias进行连表查询，相同字段请指定查询字段
$this->user->set_table_alias('a'); // set_table_alias 必须在查询最前，设置当前model的表别名就，即 user a
$this->user->select('a.uid as uid1, b.uid as uid2');
$userinfo = $this->user->join('user2 b', 'a.uid=b.uid','left')->where('b.uid', 1)->find();

// 最后判断一下，有可能返回的是false
$userinfo = is_array($userinfo) ? $userinfo : array();		
```

 **model示例中也存在有db对象**
```
// 使用$this->user->db操作，$this->user->db == $this->db
// 只不过$this->user->db 不用 $this->load->database();
$userinfo = $this->user->db->select('*')->from('user')->where('uid', 1)->get()->row_array();
```

#### 视图操作示例

1. 设置视图数据 $this->view_data
2. 设置视图文件 Template::set_view
3. 渲染视图 Template::render

 **在controller中使用** 
首页在controller中、或者其父类中载入视图渲染类库
```
$this->load->library('Template');
```

 **然后在controller方法使用如下** 
```
// 设置视图数据
$this->view_data = array('some array data');
// 设置视图文件
Template::set_view('welcome_message');
// 渲染视图
Template::render();
```

 **使用示例** 
 **布局视图layout.php** 
```
<?php Template::sub_view_render('header');?>
<?php echo $__VIEW_CONTENT__?>
<?php Template::sub_view_render('footer');?>
```

 **头部视图header.php** 
```
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title><?php Template::title(); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<?php Template::trigger_meta();?>
	<meta name="keywords" content="<?php Template::keywords();?>" />
	<meta name="description" content="<?php Template::description();?>" />
	<?php Template::trigger_css();?>
	<?php Template::trigger_js(Template::POS_HEAD);?>
</head>
<body>
```

 **尾部视图footer.php**
```
<?php Template::trigger_js(Template::POS_END);?>
</body>
</html>
```

 **主内容视图welcome_message.php** 
```
<?php 
// 设置页面title
Template::set_title('Welcome to CodeIgniter!');

// 设置页面meta
Template::meta('http-equiv-test', 'IE=edge,chrome=1,test', true);
Template::meta('generator', 'CodeIgniter 3.1.8');

// 设置页面keywords
Template::set_keywords('CodeIgniter 3.1.8');

// 设置页面description
Template::set_description('CodeIgniter is very good');

// 添加js内容到header和footer的trigger_js位置
$js_content = <<< SCT
	(function(){
		console.log('js_content');
	})();
SCT;
Template::add_js_content($js_content, Template::POS_HEAD);
Template::add_js_content($js_content, Template::POS_END);

// 添加到header的trigger_css位置
Template::add_css('style/c.css');

// 添加css内容到header的trigger_css位置
$css_content = <<< SCT
	code{
		color:#f1210a
	}
SCT;
Template::add_css_content($css_content);

// 添加到header的trigger_js位置
Template::add_js(array('javascript/path1/a.js','javascript/path1/b.js'), Template::POS_HEAD);

// 添加到footer的trigger_js位置
Template::add_js(array('javascript/path2/a.js','javascript/path2/b.js'), Template::POS_END, false);

?>
<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<?php
			// 推荐
			Template::sub_view_render('item2');
			
			// 或者
			//echo Template::sub_view_render('item2', true);
			
			/*
			或者
			foreach ($items as $value) {
				Template::sub_view_render('item', false, $value);
			}
			*/
		?>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<?php Template::sub_view_render('sub_view')?>
```

 **最后渲染出的html如下** 
```

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Welcome to CodeIgniter!</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<meta http_equiv="http-equiv-test" content="IE=edge,chrome=1,test">
<meta name="generator" content="CodeIgniter 3.1.8">
	<meta name="keywords" content="CodeIgniter 3.1.8" />
    <meta name="description" content="CodeIgniter is very good" />
	<link href="http://static.xxx.com/min/?f=style/c.css&v=1.0201805071552" rel="stylesheet" />
<style>
	code{
		color:#f1210a
	}
</style>
	<script type="text/javascript">
	(function(){
		console.log('js_content');
	})();
</script>
<script src="http://static.xxx.com/min/?f=javascript/path1/a.js,javascript/path1/b.js&v=1.0201805071552" type="text/javascript"></script>
</head>
<body>
<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

			<p>If you would like to edit this page you'll find it located at:</p>
	<code>application/views/welcome_message.php</code>
	<p>The corresponding controller for this page is found at:</p>
	<code>application/controllers/Welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>0.4321</strong> seconds. CodeIgniter Version <strong>3.1.8</strong></p>
</div>
<div id="container">
	<h1>core_function: I'm a common function</h1>
	<h1>welcome_function: I'm a application function</h1>
	<h1>I' m a common config value</h1>
</div>

<script type="text/javascript">
	(function(){
		console.log('js_content');
	})();
</script>
<script src="http://static.xxx.com/javascript/path2/a.js?v=1.0201805071552" type="text/javascript" ></script>
<script src="http://static.xxx.com/javascript/path2/b.js?v=1.0201805071552" type="text/javascript" ></script>
</body>
</html>
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
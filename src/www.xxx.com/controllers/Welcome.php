<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller 
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('welcome');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = array();
		
		// 在Base_Controller中已加载
		$data['core_function'] = core_function('core_function: I\'m a common function');
		
		// 在该控制器构造函数中已加载
		$data['welcome_function'] = welcome_function('welcome_function: I\'m a application function');
		
		/*直接load或者在admin.xxx.com/config/autoload.php中的$autoload['helper']进行设置$autoload['helper'] = array('config'); 
		 * 即：依次在admin.xxx.com/config下和在COMPATH/config文件夹中寻找config.php
		 */
		$this->load->config('config');
		$data['common_config'] = config_item('common_config');
		
		// 加载User_model，model自会加载公共数据库配置
		// $this->load->model('user_model', 'user');
		// 查找uid为1的用户，无需写->from('user')
		// $userinfo = $this->user->find(1);

		// 或者
		//$userinfo = $this->user->where('uid', 1)->find();
		// 或者 指定字段,不指定默认select(*)
		//$userinfo = $this->user->select('uid,username')->where('uid', 1)->find();
		
		// 使用model自身封装的方法
		// $userinfo = $this->user->get_user(1);
		// 使用model继承的公共model中封装的方法
		// $userinfo = $this->user->com_get_user(1);
		
		// 使用set_table_alias进行连表查询，相同字段请指定查询字段
		//$this->user->set_table_alias('a'); // set_table_alias 必须在查询最前，设置当前model的表别名就，即 user a
		//$this->user->select('a.uid as uid1, b.uid as uid2');
		//$userinfo = $this->user->join('user2 b', 'a.uid=b.uid','left')->where('b.uid', 1)->find();
		
		// 最后判断一下，有可能返回的是false
		//$userinfo = is_array($userinfo) ? $userinfo : array();		
		
		// 使用原生$this->db操作
		// 加载公共数据库配置
		// $this->load->database();
		// $userinfo = $this->db->select('*')->from('user')->where('uid', 1)->get()->row_array();
		
		// 原生$this->db连表查询
		//$this->db->select('user.uid as uid1, user2.uid as uid2');
		//$userinfo = $this->db->join('user2', 'user.uid=user2.uid','left')->where('user2.uid', 1)->get()->row_array();
		
		// 或者 使用$this->user->db操作，$this->user->db == $this->db
		// 只不过$this->user->db 不用 $this->load->database();
		// $userinfo = $this->user->db->select('*')->from('user')->where('uid', 1)->get()->row_array();
		
		//$this->load->view('welcome_message', $data);

		$data['items'][] = array('title'=>"If you would like to edit this page you'll find it located at", 'code'=>'application/views/welcome_message.php');
		$data['items'][] = array('title'=>"The corresponding controller for this page is found at", 'code'=>'application/controllers/Welcome.php');
		
		$this->view_data = $data;
		Template::set_view('welcome_message');
		Template::render();
	}

	function test()
	{
		$this->load->library('hulk_template');
		
		$this->hulk_template->parse('test_message');
	}
}

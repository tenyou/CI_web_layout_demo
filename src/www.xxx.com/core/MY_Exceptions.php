<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require COMPATH."core/Base_Exceptions.php";

class MY_Exceptions extends Base_Exceptions {}

/* End of file MY_Exceptions.php */
/* Location: ./application/core/MY_Exceptions.php */
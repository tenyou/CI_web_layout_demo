<?php
/**
 * 以表名命名的model，继承公共model  Com_user_model，若没有公共model ，可直接继承 MY_Model
 * 表名：xxx_user
 * model 会自动加载公共数据库配置
 */

class User_model extends Com_user_model
{
	/**
	 * Table name
	 *
	 * @var string
	 * @access protected
	 */
	public static $table_name = 'user';

	/**
	 *Table primary key, if no default is used "Id".
	 *
	 * @var string
	 * @access protected
	 */
	protected $key = 'uid';
	
	function get_user($uid)
	{
		return $this->find($uid);
	}
}


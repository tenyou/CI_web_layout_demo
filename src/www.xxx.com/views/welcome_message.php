<?php 
// 设置页面title
Template::set_title('Welcome to CodeIgniter!');

// 设置页面meta
Template::meta('http-equiv-test', 'IE=edge,chrome=1,test', true);
Template::meta('generator', 'CodeIgniter 3.1.8');

// 设置页面keywords
Template::set_keywords('CodeIgniter 3.1.8');

// 设置页面description
Template::set_description('CodeIgniter is very good');

// 添加js内容到header和footer的trigger_js位置
$js_content = <<< SCT
	(function(){
		console.log('js_content');
	})();
SCT;
Template::add_js_content($js_content, Template::POS_HEAD);
Template::add_js_content($js_content, Template::POS_END);

// 添加到header的trigger_css位置
Template::add_css('style/c.css');

// 添加css内容到header的trigger_css位置
$css_content = <<< SCT
	code{
		color:#f1210a
	}
SCT;
Template::add_css_content($css_content);

// 添加到header的trigger_js位置
Template::add_js(array('javascript/path1/a.js','javascript/path1/b.js'), Template::POS_HEAD);

// 添加到footer的trigger_js位置
Template::add_js(array('javascript/path2/a.js','javascript/path2/b.js'), Template::POS_END, false);

?>
<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<?php
			// 推荐
			Template::sub_view_render('item2');
			
			// 或者
			//echo Template::sub_view_render('item2', true);
			
			/*
			或者
			foreach ($items as $value) {
				Template::sub_view_render('item', false, $value);
			}
			*/
		?>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<?php Template::sub_view_render('sub_view')?>
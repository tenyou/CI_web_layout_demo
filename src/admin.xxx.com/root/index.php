<?php
/*
 *---------------------------------------------------------------
 * 应用程序文件夹名称
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder then the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server.  If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 *
 */

/**
 * 当前站点目录名
 * @var string
 */
define('SITENAME', pathinfo(dirname(__DIR__), PATHINFO_BASENAME));

/**
 * 要加载的CI版本，system/codeigniter下CI的版本号文件夹名
 * @var unknown
 */
define('CI_VER', '3.1.8');

/*
 * --------------------------------------------------------------------
 * 加载公共文件
 * --------------------------------------------------------------------
 *
 */

require_once dirname(dirname(dirname(__DIR__))). '/common/index.php';
/* End of file index.php */
/* Location: ./root/index.php */
<?php
/**
 * 以表名命名的model，继承公共model  Com_user_model，若没有公共model ，可直接继承 MY_Model
 * 表名：xxx_user
 * model 会自动加载公共数据库配置
 */

class User_model extends Com_user_model
{
	/**
	 * Database connection configuration
	 * default 可设置为主库，slave为从库
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $db_group = 'slave';

	/**
	 * 是否返回最后新增的ID，即insert后返回insert_id
	 *
	 * @var bool
	 */
	protected $return_insert_id = FALSE;
	
	function get_user($uid)
	{
		return $this->find($uid);
	}
}


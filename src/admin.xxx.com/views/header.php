<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title><?php Template::title(); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<?php Template::trigger_meta();?>
	<meta name="keywords" content="<?php Template::keywords();?>" />
	<meta name="description" content="<?php Template::description();?>" />
	<?php Template::trigger_css();?>
	<?php Template::trigger_js(Template::POS_HEAD);?>
</head>
<body>